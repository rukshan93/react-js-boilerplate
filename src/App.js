import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useSelector, useDispatch } from 'react-redux';
import actions from './redux/actions';

function App() {

  const defaultReduxState = useSelector(state => state.default.count);
  const defaultUsersThunk = useSelector(state => state.default.users);

  const dispatch = useDispatch();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

        <p>Count : {defaultReduxState}</p>

        <button onClick={() => { dispatch(actions.defaultActions.incrementCount()) }}>Increment Count</button>

        <hr />

        <p>Fetched Users : {defaultUsersThunk.length}</p>


        <button onClick={() => { dispatch(actions.defaultActions.fetchAjaxData()) }}>Fetch AJAX Data</button>

      </header>
    </div>
  );
}

export default App;
