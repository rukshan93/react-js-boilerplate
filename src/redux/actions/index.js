import actionTypes from "../action-types"
import Axios from "axios"

const defaultActions = {
    incrementCount : () => {
        return {
            type : actionTypes.defaultActions.INCREMENT_COUNT
        }
    },

    fetchAjaxData : () => {
        return (dispatch) => {
            Axios.get("https://jsonplaceholder.typicode.com/users").then(data => {
                return dispatch( defaultActions.fetchAjaxDataComplete(data.data))
            }).catch( e => {
                return dispatch( defaultActions.fetchAjaxDataError())
            });
        }
    },

    fetchAjaxDataComplete : (data) => {
        return {
            type : actionTypes.defaultActions.FETCH_USERS,
            payload : data
        }
    },

    fetchAjaxDataError : () => {
        return {
            type : actionTypes.defaultActions.FETCH_USERS,
            payload : []
        }
    }
}

export default {
    defaultActions
}