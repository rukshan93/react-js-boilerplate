import actionTypes from "../action-types"
import actions from "../actions";

export default {
    default: (state = { count: 0, users: [] }, action) => {
        switch (action.type) {
            case actionTypes.defaultActions.INCREMENT_COUNT: return { ...state, count: state.count + 1 };
            case actionTypes.defaultActions.FETCH_USERS: return { ...state, users: action.payload };
            default: return state
        }
    }
}