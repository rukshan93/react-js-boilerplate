import { } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import reducers from '../reducers';
import thunk from 'redux-thunk';

const stores =
    createStore(
        combineReducers(
            reducers
        ),applyMiddleware(thunk));

export default stores;
