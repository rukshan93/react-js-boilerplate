export default{
    defaultActions : {
        INCREMENT_COUNT : 'INCREMENT_COUNT',
        FETCH_USERS : 'FETCH_USERS',
    }
}